# OpenML dataset: Myanmar-Air-Quality(2019-to-2020-Oct)

https://www.openml.org/d/43748

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Since Myanmar is one of the developing countries, a lot of factories were set up and the number of cars increased speedily during the previous years. Therefore, Myanmar's air quality was also dramatically decreasing during the last years. Moreover,  Myanmar air quality reached no.4 in the worst air quality globally in 2019. So, I created this dataset to analyze and to try some predictions.
Content
Data is from Purple.com and cleaned by using PowerBI. 
Acknowledgements
This dataset is a part of the project which is initialized to compete Myanmar's air quality visualization competitions. So, I would like to give credits to my friends who participated in that competition with me. 
Inspiration
I hope this dataset can help the field of data science and the air quality of Myanmar. Context
Since Myanmar is one of the developing countries, a lot of factories were set up and the number of cars increased speedily during the previous years. Therefore, Myanmar's air quality was also dramatically decreasing during the last years. Moreover,  Myanmar air quality reached no.4 in the worst air quality globally in 2019. So, I created this dataset to analyze and to try some predictions.
Content
Data is from PurpleAir.com and cleaned by using PowerBI. 
Acknowledgements
This dataset is a part of the project which is initialized to compete Myanmar's air quality visualization competitions. So, I would like to give credits to my friends who participated in that competition with me. 
Inspiration
I hope this dataset can help the field of data science and the air quality of Myanmar. Context
Since Myanmar is one of the developing countries, a lot of factories were set up and the number of cars increased speedily during the previous years. Therefore, Myanmar's air quality was also dramatically decreasing during the last years. Moreover,  Myanmar air quality reached no.4 in the worst air quality globally in 2019. So, I created this dataset to analyze and to try some predictions.
Content
Data is from PurpleAir.com and cleaned by using PowerBI. 
Acknowledgements
This dataset is a part of the project which is initialized to compete Myanmar's air quality visualization competitions. So, I would like to give credits to my friends who participated in that competition with me. 
Inspiration
I hope this dataset can help the field of data science and the air quality of Myanmar.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43748) of an [OpenML dataset](https://www.openml.org/d/43748). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43748/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43748/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43748/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

